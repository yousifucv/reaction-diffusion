package reactiondiffuse

object sheet {
  val x = 1                                       //> x  : Int = 1
  val y = 2                                       //> y  : Int = 2
  for (iy <- 0 to 2; ix <- 0 to 2) {
    val gx = x - 1 + ix
    val gy = y - 1 + iy

    if (ix != 1 && iy != 1)
      println(ix + " " + iy)
      
      
    if (!(ix == 1 && iy == 1))
      println(ix + " " + iy)
  }                                               //> 0 0
                                                  //| 0 0
                                                  //| 1 0
                                                  //| 2 0
                                                  //| 2 0
                                                  //| 0 1
                                                  //| 2 1
                                                  //| 0 2
                                                  //| 0 2
                                                  //| 1 2
                                                  //| 2 2
                                                  //| 2 2
}