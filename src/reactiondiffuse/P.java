
package reactiondiffuse;

import processing.core.PApplet;
import processing.event.MouseEvent;

/*
 *
 */
public class P extends PApplet {

    public static ScalaMain s;
    public static JavaMain j;

    static int SCREEN_WIDTH = 0;
    static int SCREEN_HEIGHT = 0;
    static boolean isFullScreen = false;

    public static void main(String args[]) {
        if (isFullScreen) {
            PApplet.main(new String[]{"--present", "reactiondiffuse.P"});
        } else {
            PApplet.main(new String[]{"reactiondiffuse.P"});
        }
    }

    public void setup() {
        if (isFullScreen) {
            size(displayWidth, displayHeight, P3D);
        } else {
            size(600, 600, P2D);
            //size(1800, 1012, P3D);
        }
        noCursor();
        //setup() starts here 
        s = new ScalaMain();
        //s.setup(this);
        j = new JavaMain();
        j.setup(this);
    }

    public void draw() {
       background(0);
       //s.draw(this);
       j.draw(this);
       if (key == 'r') saveFrame("frames/####.png");
    }
    
    public void mousePressed(MouseEvent event) {
    	super.mousePressed(event);
    	j.mousePressed(event, this);
    }
    
    public void mouseDragged(MouseEvent event) {
    	super.mouseDragged(event);
    	j.mouseDragged(event, this);
    }
    
    //prints framerate in the top left
    void showFrameRate() {
        camera(width / 2, height / 2, (height / 2) / tan(PI / 6), width / 2, height / 2, 0, 0, 1, 0);
        fill(125);
        text(frameRate, 20, 20);
    }

    //draws a small square mouseCursor
    void updateMouse() {
        camera(width / 2, height / 2, (height / 2) / tan(PI / 6), width / 2, height / 2, 0, 0, 1, 0);
        noStroke();
        fill(125, 125, 125, 150);
        rect(mouseX - 2, mouseY - 2, 4, 4);
    }

    //draws an axis with a specified length, and location
    void drawAxis(int length, int x, int y, int z) {
        translate(x, y, z);
        drawAxis(length);
        translate(-x, -y, -z);
    }

    //draws an axis with specified length
    private void drawAxis(int length) {
        stroke(255, 0, 0);
        fill(255, 0, 0);
        line(-length / 2, 0, 0, length / 2, 0, 0);//xaxis
        translate(length / 2, 0, 0);
        fill(255, 100, 100);
        box(10, 10, 10);
        translate(-length / 2, 0, 0);

        stroke(0, 255, 0);
        fill(0, 255, 0);
        line(0, -length / 2, 0, 0, length / 2, 0);//yaxis
        translate(0, length / 2, 0);
        fill(200, 255, 200);
        box(10, 10, 10);
        translate(0, -length / 2, 0);

        stroke(0, 0, 255);
        fill(0, 0, 255);
        line(0, 0, -length / 2, 0, 0, length / 2);//zaxis
        translate(0, 0, length / 2);
        fill(100, 100, 255);
        box(10, 10, 10);
        translate(0, 0, -length / 2);
    }

}
