package reactiondiffuse;

public class JGrid {

	int width;
	int height;
	Cell[][] cells;

	Selector selectA = new Selector() {
		public float get(Cell c) {
			return c.a;
		}
	};

	Selector selectB = new Selector() {
		public float get(Cell c) {
			return c.b;
		}
	};

	public JGrid(int width, int height) {
		this.width = width;
		this.height = height;
		cells = new Cell[height][width];
		for(int j = 0; j < cells.length; j++) {
			for(int i = 0; i < cells[j].length; i++) {
				cells[j][i] = newCell();
			}
		}
	}

	public Cell newCell() {
		return new Cell(1f, 0f);
	}


	float[][] kernel = {
			{.05f, .2f, .05f},
			{.20f, -1f, .20f},
			{.05f, .2f, .05f}
	};

	float[][] kernel2 = {
			{.00f, .25f, .00f},
			{.25f, -1f, .25f},
			{.00f, .25f, .00f}
	};

	float[][] kernel3 = {
			{.00f, 1f, .00f},
			{1f, -4f, 1f},
			{.00f, 1f, .00f}
	};

	public float convolve(int x, int y, Selector selector)  {
		float sum = 0f;
		float totalWeight = 0f;

		for(int iy = 0; iy <= 2; iy++) {
			for(int ix = 0; ix <= 2; ix++) {

				int gx = x - 1 + ix;
				int gy = y - 1 + iy;

				if (!(ix == 1 && iy == 1) && gx >= 0 && gx < width && gy >= 0 && gy < height) {
					float weight = kernel3[iy][ix];
					sum += selector.get(cells[gy][gx]) * weight;
					totalWeight += weight;
				}
			}
		}

		return sum + selector.get(cells[x][y]) * -totalWeight;
	}

	public float convolve2(int x, int y, Selector selector)  {

		int left = Math.max(0, x - 1);
		int right = Math.min(x + 1, width - 1);
		int up = Math.max(0, y - 1);
		int down = Math.min(y + 1, height - 1);

		Selector s = selector;
		return s.get(cells[y][left]) + s.get(cells[y][right]) + s.get(cells[up][x]) + s.get(cells[down][x]) - 4 * s.get(cells[y][x]);
	}
	
	public float convolveA(int x, int y )  {

		int left = Math.max(0, x - 1);
		int right = Math.min(x + 1, width - 1);
		int up = Math.max(0, y - 1);
		int down = Math.min(y + 1, height - 1);

		return  cells[y][left].a +  cells[y][right].a +  cells[up][x].a +  cells[down][x].a - 4 *  cells[y][x].a;
	}
	
	public float convolveB(int x, int y )  {

		int left = Math.max(0, x - 1);
		int right = Math.min(x + 1, width - 1);
		int up = Math.max(0, y - 1);
		int down = Math.min(y + 1, height - 1);

		return  cells[y][left].b +  cells[y][right].b +  cells[up][x].b +  cells[down][x].b - 4 * cells[y][x].b;
	}

	public void step(float t) {
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				Cell cell = cells[y][x];
				float reactionTerm = (cell.a * cell.b * cell.b);
				cell.newA = cell.a + (Params.diffusionA * convolve2(x, y, selectA) - reactionTerm + Params.feed * (1 - cell.a)) * t;
				cell.newB = cell.b + (Params.diffusionB * convolve2(x, y, selectB) + reactionTerm - (Params.kill + Params.feed) * cell.b) * t;
			}
		}

		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				cells[y][x].update();
			}
		}
	}

	interface Selector {
		public float get(Cell c);
	}

	public class Cell {

		float a;
		float b;
		float newA;
		float newB;

		public Cell(float a, float b) {
			this.a = a;
			this.b = b;
		}
		public void update() {
			a = newA;
			b = newB;
		}
	}

	static class Params {
		//Mitosis
//		public static float diffusionA =0.14f;
//		public static float diffusionB = 0.06f;
//		public static float feed = .035f;
//		public static float kill = .065f;
		
		//Spiral
		public static float diffusionA = 0.1f;
		public static float diffusionB = 0.08f;
		public static float feed = 0.02f;
		public static float kill = 0.05f;

		//Coral?
//	   public static float diffusionA = 0.16f;
//	   public static float diffusionB = 0.08f;
//	   public static float feed = 0.035f;
//	   public static float kill = 0.06f;
		 
	}

}
