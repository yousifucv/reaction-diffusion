package reactiondiffuse;

import java.util.Random;

import processing.core.PApplet;
import processing.event.MouseEvent;

public class JavaMain {

	long nanoBefore = 0L;
	long nanoAfter = 0L;

	JGrid grid = null;

	public void setup(PApplet p) {

		grid = new JGrid(200, 200);
		Random r = new Random();

	}

	public void draw(PApplet p) {
		//System.out.println(p.frameRate);

		nanoBefore = System.nanoTime();
		for(int steps = 0; steps < 20; steps++)
			grid.step(1f);
		//System.out.println("Stepping: " + ((System.nanoTime() - nanoBefore) / 1000000) + " ms");

		nanoBefore = System.nanoTime();
		p.loadPixels();
		for(int y = 0; y < p.height; y++) {
			for(int x = 0; x < p.width; x++) {
				int mappedX = (int)Math.round(PApplet.map(x, 0, p.width, 0, grid.width - 1 ));
				int mappedY = (int)Math.round(PApplet.map(y, 0, p.height, 0, grid.height - 1));
				JGrid.Cell c = grid.cells[mappedY][mappedX];

				int aColor = (int)(255 * c.a / (c.a + c.b)) << 16;
				int bColor = (int)(255 * c.b / (c.a + c.b));

				p.pixels[y * p.width + x] = p.lerpColor(0xFFA7A37E, 0xFFE6E2AF, 1 - c.a);
			}
		}
		p.updatePixels();
		//System.out.println("Painting: " + ((System.nanoTime() - nanoBefore) / 1000000) + " ms");
	}

	public void mousePressed(MouseEvent event, PApplet p) {
	}

	public void mouseDragged(MouseEvent event, PApplet p) {
		int mx = event.getX();
		int my = event.getY();

		int mappedX = (int)PApplet.map(mx, 0, p.width, 0, grid.width);
		int mappedY = (int)PApplet.map(my, 0, p.height, 0, grid.height);
		
		int jStart = Math.max(0, mappedY - 5);
		int jEnd = Math.min(grid.height, mappedY + 5);
		int iStart = Math.max(0, mappedX - 5);
		int iEnd = Math.min(grid.width, mappedX + 5);

		for (int j = jStart; j < jEnd; j++) {
			for (int i = iStart; i < iEnd; i++) {    
				if(j > 0 || j < grid.height || i > 0 || i < grid.width) {
					grid.cells[j][i].a = 0.5f;
					grid.cells[j][i].b = 0.25f;
				}
			}
		}
	}
}
