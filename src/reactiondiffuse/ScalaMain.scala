package reactiondiffuse

import reactiondiffuse._
import processing.core.PApplet
import scala.util.Random

class ScalaMain {

  var grid: Grid = null;

  def setup(p: PApplet) {

    grid = new Grid(p.width, p.height)
    val r = new Random();
    for (i <- 1 to 10) {
      grid.cells(r.nextInt(p.width))(r.nextInt(p.height)).b = 1
    }
  }

  def draw(p: PApplet) {

    for (k <- 1 to 100)
      grid.step(1f)

    p.loadPixels()
    for (x <- 0 until p.width; y <- 0 until p.height) {

      val c = grid.cells(x)(y)

      val aColor = (255 * c.a / (c.a + c.b)).asInstanceOf[Int] << 16
      val bColor = (255 * c.b / (c.a + c.b)).asInstanceOf[Int]

      p.pixels(y * p.width + x) = p.lerpColor(0xFF0000FF, 0xFFFFFF00, 1 - c.a)

      //println(c.a + " " + c.b)
    }
    p.updatePixels()

  }

}