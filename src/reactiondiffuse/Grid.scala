package reactiondiffuse

class Grid(val width: Int, val height: Int) {
  def newCell = new Cell(1, 0)

  val cells = Array.fill[Cell](width, height)(newCell)

  val kernel = Array(
    Array(.05f, .2f, .05f),
    Array(.20f, -1f, .20f),
    Array(.05f, .2f, .05f))

  val kernel2 = Array(
    Array(.00f, .25f, .00f),
    Array(.25f, -1f, .25f),
    Array(.00f, .25f, .00f))

  val kernel3 = Array(
    Array(0f, 1f, 0f),
    Array(1f, -4f, 1f),
    Array(0f, 1f, 0f))

  def convolve(x: Int, y: Int, selector: Cell => Float) = {
    var sum = 0f
    var totalWeight = 0f

    for (iy <- 0 to 2; ix <- 0 to 2) {
      val gx = x - 1 + ix
      val gy = y - 1 + iy

      if (!(ix == 1 && iy == 1) && gx >= 0 && gx < width && gy >= 0 && gy < height) {
        val weight = kernel3(iy)(ix)
        sum += selector(cells(gx)(gy)) * weight
        totalWeight += weight
      }
    }
    sum + selector(cells(x)(y)) * -totalWeight
  }

  def convolve2(x: Int, y: Int, selector: Cell => Float) = {

    val c = cells(x)(y)
    var sum = 0f;

    val left = Math.max(0, x - 1)
    val right = Math.min(x + 1, width - 1)
    val up = Math.max(0, y - 1)
    val down = Math.min(y + 1, height - 1)

    selector(cells(left)(y)) + selector(cells(right)(y)) + selector(cells(x)(up)) + selector(cells(x)(down)) - 4 * selector(cells(x)(y))
  }

  def step(t: Float) {
    for (y <- 0 until height; x <- 0 until width) {
      val cell = cells(x)(y)
      val reactionTerm = (cell.a * cell.b * cell.b);
      cell.newA = cell.a + (Params.diffusionA * convolve2(x, y, _.a) - reactionTerm + Params.feed * (1 - cell.a)) * t
      cell.newB = cell.b + (Params.diffusionB * convolve2(x, y, _.b) + reactionTerm - (Params.kill + Params.feed) * cell.b) * t
    }

    for (y <- 0 until height; x <- 0 until width) cells(x)(y).update
  }

}

class Cell(var a: Float, var b: Float) {

  var newA = 0f
  var newB = 0f

  def update {
    a = newA
    b = newB
  }

}

object Params {
  val diffusionA = 0.16f
  val diffusionB = 0.08f

  val feed = 0.0545f //.0367f
  val kill = 0.062f //.0649f

  /*
   diffU = 0.16;
   diffV = 0.08;
   paramF = 0.035;
   paramK = 0.06;
   */
}




